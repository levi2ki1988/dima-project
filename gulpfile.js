'use strict';

// INIT
const gulp = require('gulp');
const less = require('gulp-less');
const connect = require('gulp-connect');
const watch = require('gulp-watch');
const clean = require('gulp-clean');
const gulpsync = require('gulp-sync')(gulp);
//const fileinclude = require('gulp-file-include');
const htmlbeautify = require('gulp-html-beautify');
const pug = require('gulp-pug2');

// PATH
var postfix = "**/*";
var path = {
    dist: 'develop/dist/',
    src: 'develop/src/',
    // less/css
    less: 'develop/src/less/',
    css: 'develop/dist/css',
    // templates
    templates: 'develop/src/templates/',
    // js
    js: 'develop/src/js/',
    // static resources
    fonts: 'develop/src/fonts/',
    media: 'develop/src/img/',
    vendor: 'develop/src/vendor'
}

// TASKS

// default
var defaultOpts = {
    tasks: ['watch', 'connect']
}
gulp.task('default', gulpsync.sync(defaultOpts.tasks));

/* 
 * connect
 * start simple webserver
 */
var connectOpts = {
    root: path.dist,
    port: 3000,
    livereload: false
};
gulp.task('connect', function () {
    connect.server(connectOpts);
});

/*
 * clean
 * clean compiled Files
 */
var cleanOpts = {
    root: path.dist + "*",
    read: false
};
gulp.task('clean', function () {
    return gulp.src(cleanOpts.root, {
        read: cleanOpts.read
    }).pipe(clean());
});

/*
 * less 
 * compile less files
 */
var lessOpts = {
    root: path.less,
    output: path.css,
    basefile: '*.less'
};
gulp.task('less', function () {
    gulp.src(lessOpts.root + lessOpts.basefile)
        .pipe(less())
        .pipe(gulp.dest(lessOpts.output));
});

/*
 * file-include 
 * making output html files from templates
 */
//var fileincludeOpts = {
//    root: path.src,
//    output: path.dist,
//    format: '*.html',
//    otps: {
//        prefix: '@@',
//        basepath: '@file'
//    }
//
//};
//gulp.task('fileinclude', function () {
//    return gulp.src([fileincludeOpts.root + fileincludeOpts.format])
//        .pipe(fileinclude(fileincludeOpts.otps))
//        .pipe(htmlbeautify(htmlbeautifyOptions))
//        .pipe(gulp.dest(fileincludeOpts.output));
//});

/*
 * pug
 * cimpile pug files
 */
var pugOpts = {
    root: path.src,
    output: path.dist,
    format: '*.pug',
    index: "index.pug",
    exclude: path.src + "templates/_*.pug",
    opts: { doctype: "html"
          }
};
gulp.task('pug', function () {
    return gulp.src(pugOpts.root + pugOpts.index)
            .pipe(pug(pugOpts.opts))
            .pipe(htmlbeautify(htmlbeautifyOptions))
            .pipe(gulp.dest(pugOpts.output));
});

/*
 * html-beautify
 * beautify compiled html
 */
var htmlbeautifyOptions = {
    "indent_size": 4,
    "indent_char": " ",
    "eol": "\n",
    "indent_level": 0,
    "indent_with_tabs": false,
    "preserve_newlines": true,
    "max_preserve_newlines": 10,
    "jslint_happy": false,
    "space_after_anon_function": false,
    "brace_style": "collapse",
    "keep_array_indentation": false,
    "keep_function_indentation": false,
    "space_before_conditional": true,
    "break_chained_methods": false,
    "eval_code": false,
    "unescape_strings": false,
    "wrap_line_length": 0,
    "wrap_attributes": "auto",
    "wrap_attributes_indent_size": 4,
    "end_with_newline": true
};

/*
 * copy-static-resources
 * copying static resources without any ops
 */
var staticOpts = {
    img: {
        root: path.media + postfix,
        output: path.dist + "img"
    },
//    fonts: {
//        root: path.fonts + postfix,
//        output: path.dist + "fonts"
//    },
    js: {
        root: path.js + postfix,
        output: path.dist + "js"
    },
    vendor: {
        root: path.vendor + postfix,
        output: path.dist
    }
};
gulp.task('img', function () {
    gulp.src(staticOpts.img.root).pipe(gulp.dest(staticOpts.img.output));
});
//gulp.task('fonts', function () {
//    gulp.src(staticOpts.fonts.root).pipe(gulp.dest(staticOpts.fonts.output));
//});
gulp.task('js', function () {
    gulp.src(staticOpts.js.root).pipe(gulp.dest(staticOpts.js.output));
});
gulp.task('vendor', function () {
    gulp.src(staticOpts.vendor.root).pipe(gulp.dest(staticOpts.vendor.output));
});

/*  
 * compile
 * basic project compiler
 */
gulp.task('compile', gulpsync.sync([
    'less',
//    'fonts',
    'pug',
//    'fileinclude',
    'img',
    'js',
    'vendor'
]));

/*
 * watch
 * watch changes and recompile
 */
var watchOpts = {
    root: [path.src + postfix],
    tasks: ['clean' ,'compile']

};
gulp.task('watch', function () {
    gulp.watch(watchOpts.root, gulpsync.sync(watchOpts.tasks));
});
