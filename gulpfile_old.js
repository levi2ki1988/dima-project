'use strict';

var gulp = require('gulp');
//var debug = require('gulp-debug-streams');
var clean = require('gulp-clean');
var gulpsync = require('gulp-sync')(gulp);
var watch = require('gulp-watch');
var htmlbeautify = require('gulp-html-beautify');
var beautify = require('gulp-beautify');
var fileinclude = require('gulp-file-include');
var connect = require('gulp-connect');
var less = require('gulp-less');

//path and options
var path = {
    clean: 'develop/app/*',
    fileinclude: {
        src: 'develop/sources/*.html',
        dest: 'develop/app/'
    },
    watch: {
        html: 'develop/sources/**/*.*'
    },
    beautify: {
        //                src: path.fileinclude.dest + '*.html',
        dest: ""
    },
    jsbeautify: {
        src: 'develop/sources/js/**/*.js',
        dest: 'develop/app/js/'

    },
    assets: {
        src: 'develop/sources/assets/**/*.*',
        dest: 'develop/app/assets/'
    }
};

var htmlbeautifyOptions = {
    "indent_size": 4,
    "indent_char": " ",
    "eol": "\n",
    "indent_level": 0,
    "indent_with_tabs": false,
    "preserve_newlines": true,
    "max_preserve_newlines": 10,
    "jslint_happy": false,
    "space_after_anon_function": false,
    "brace_style": "collapse",
    "keep_array_indentation": false,
    "keep_function_indentation": false,
    "space_before_conditional": true,
    "break_chained_methods": false,
    "eval_code": false,
    "unescape_strings": false,
    "wrap_line_length": 0,
    "wrap_attributes": "auto",
    "wrap_attributes_indent_size": 4,
    "end_with_newline": true
};

////TASKS
//server
/*
 * run server
 */
gulp.task('connect', function () {
    connect.server({
        root: 'develop/app/',
        port: 3000,
        livereload: false
    });
});

//clean
/*
 * clean dist directory tecursively
 */
gulp.task('clean', function () {
    return gulp.src(path.clean, {
            read: false
        })
        .pipe(clean());
});

// prettify tasks
gulp.task('jsbeautify', function () {
    return gulp.src(path.jsbeautify.src)
        .pipe(beautify({
            indent_size: 4
        }))
        .pipe(gulp.dest(path.jsbeautify.dest));
});

// less
gulp.task('less', function () {
    gulp.src('develop/sources/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('develop/app/css'));
});


//file-include
/*
 * making output html files from templates
 */
gulp.task('fileinclude', function () {
    return gulp.src([path.fileinclude.src])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(htmlbeautify(htmlbeautifyOptions))
        .pipe(gulp.dest(path.fileinclude.dest));
});


//copy-static-resources
gulp.task('vendor', function () {
    gulp.src('develop/sources/vendor/**/*')
        .pipe(gulp.dest('develop/app/vendor'))
});
gulp.task('img', function () {
    gulp.src('develop/sources/img/**/*.*')
        .pipe(gulp.dest('develop/app/img'));
});

//compile
/*
 * basic html cleaner + compiler
 */
gulp.task('compile', gulpsync.sync([
//    'clean',
    'vendor',
    'fileinclude',
    'img',
    'less',
    'jsbeautify'
]));


//watch
/*
 * watch section
 */
gulp.task('watch', function () {
    gulp.watch([path.watch.html, '!develop/sources/css/**/*'], gulpsync.sync(['clean', 'compile']));
});


gulp.task('default', gulpsync.sync([
    'watch',
    'connect'
]));
