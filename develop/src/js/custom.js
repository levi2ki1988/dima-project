/* Add here all your JS customizations */

//////////////////////////////
// -- WOW.js Animations -- //
////////////////////////////
$(document).ready(function () {
    //    var wow = new WOW()
    new WOW({
        offset: 15
    }).init();
})


////////////////////////////
// -- Video Backgound -- //
//////////////////////////
const backgroundVideo = new BackgroundVideo('.bv-video', {
    src: [
            '/img/intro/video-bg/camera.mp4'
        ],
    parallax: {
        effect: 3
    }
});